        //Datatable
        $(document).ready(function () {
        jQuery('#btnLoadSKU').click(function (event) {
        $('#popup').modal('show');
        });
        jQuery('#btnloadtosku').click(function (event) {
        $('#popup').modal('show');
        });
        jQuery('#btnloadbatch').click(function (event) {
        $('#popup').modal('show');
        });
        jQuery('#btnloadQty').click(function (event) {
        $('#popup').modal('show');
        });
        jQuery('#btnloadexpiry').click(function (event) {
        $('#popup').modal('show');
        });
        jQuery('#btnloadmanufacturing').click(function (event) {
        $('#popup').modal('show');
        });
        jQuery('#btnSubmit').click(function (event) {
        $('#Alertpopup').modal('show');
        });
        $('input[type="checkbox"]').on('change', function () {
        var tdColumn = $(this).closest("td");
        var trRow = tdColumn.closest("tr");
        trRow.find(":radio").not($(this)).prop("checked", false);
        var i = tdColumn.index();
        trRow.siblings("tr").each(function (key, value) { $(value).find('input[type="checkbox"]')[i].checked = false; });
        });
        //checkbox navigaton in table
        $('#Checkboxheader').change(function () {
        $('#tblfromSKUDetails tbody tr td input[type="checkbox"]').prop('checked', $(this).prop('checked'));
        });
        //Export table Excel
        jQuery('#btnExportSKUDetails').click(function (event) {
        $('#tblSKUDetails').tableExport({ type: 'excel', escape: 'false' });
        });
        //Export table PDF
        jQuery('#btnExportSKUDetailstoPDF').click(function (event) {
        $('#tblSKUDetails').tableExport({ type: 'pdf', pdfFontSize: '7', escape: 'false' });
        });
        //Export table Excel
        jQuery('#btnExportTransferDetails').click(function (event) {
        $('#tblTransferDetails').tableExport({ type: 'excel', escape: 'false' });
        });
        //Export table PDF
        jQuery('#btnExportTransferDetailstoPDF').click(function (event) {
        $('#tblTransferDetails').tableExport({ type: 'pdf', pdfFontSize: '7', escape: 'false' });
        });
        //checkbox navigaton in table
        $('#chkHeader').change(function () {
        $('#tblTransferDetails tbody tr td input[type="checkbox"]').prop('checked', $(this).prop('checked'));
        });
        $('#tblTransferDetails tbody tr td input:checkbox').change(function () {
        $('#chkHeader').prop('checked', $('td input:checkbox:checked').length == $('td input:checkbox').length);
        });
        //Take sku details from popup and append to the main table
        $('#btnSubmitpop').click(function () {
        $('#tblSKUDetails').find('tr').not(":first").each(function () {
        if ($(this).find('input[type="checkbox"]').is(':checked')) {
        var fromsku = $(this).find('td:eq(1)').text();
        var tosku = $(this).find('td:eq(1)').text();
        var batch = $(this).find('td:eq(3)').text();
        var qty = $(this).find('td:eq(6)').text();
        var manufacturedate = $(this).find('td:eq(4)').text();
        var expirydate = $(this).find('td:eq(5)').text();
        $('#txtFromSKU').val(fromsku);
        $('#txtToSKU').val(fromsku);
        $('#txtFromBatch').val(batch);
        $('#txtFromQty').val(qty);
        $('#txtFromManuDate').val(manufacturedate);
        $('#txtFromExpiryDate').val(expirydate);
        $('#popup').modal('hide');
        }
        });
        });
        var i = 1;
        $('#btnAddDetails').click(function () {
        i++;
        var fromsku = $('#txtFromSKU').val();
        var tosku = $('#txtToSKU').val();
        var frombatch = $('#txtFromBatch').val();
        var tobatch = $('#txtToBatch').val();
        var fromqty = $('#txtFromQty').val();
        var toqty = $('#txtToQty').val();
        var frommanufacturedate = $('#txtFromManuDate').val();
        var tomanufacturedate = $('#txtToManuDate').val();
        var fromexpirydate = $('#txtFromExpiryDate').val();
        var toexpirydate = $('#txtToExpiryDate').val();
        if (fromsku != '' && tosku != '' && frombatch != '' && tobatch != '' && frommanufacturedate != '' && tomanufacturedate != '' && fromexpirydate != '' && toexpirydate != '') {
        var newRow = jQuery('<tr><td><input id="checkbox' + i + '" type="checkbox" /><label for="checkbox' + i + '">Select</label></td><td>' + fromsku + '</td><td>' + tosku + '</td><td>' + frombatch + '</td><td>' + tobatch + '</td><td>' + fromqty + '</td><td>' + toqty + '</td><td>' + fromexpirydate + '</td><td>' + toexpirydate + '</td><td>' + frommanufacturedate + '</td><td>' + tomanufacturedate + '</td></tr>');
        jQuery('#tblTransferDetails').append(newRow);
        $('#txtFromSKU').val('');
        $('#txtToSKU').val('');
        $('#txtFromBatch').val('');
        $('#txtToBatch').val('');
        $('#txtFromQty').val('');
        $('#txtToQty').val('');
        $('#txtFromManuDate').val('');
        $('#txtToManuDate').val('');
        $('#txtFromExpiryDate').val('');
        $('#txtToExpiryDate').val('');
        }
        });
        });
