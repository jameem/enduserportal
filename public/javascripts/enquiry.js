//Datatable
$(document).ready(function () {
      var url = window.location;
      // for sidebar menu entirely but not cover treeview
      $('ul.sidebar-menu a').filter(function () {
      return this.href == url;
      }).parent().addClass('active');
      // for treeview
      $('ul.treeview-menu a').filter(function () {
      return this.href == url;
      }).closest('.treeview').addClass('active');
      $("#lnkEdit").addClass('disabled');
      $('#example').DataTable();
      //Select table row on click
      $("#example tr").click(function (e) {
      var selected = $(this).hasClass("highlight");
      $("#example tr").removeClass("highlight");
      if (!selected)
      $(this).addClass("highlight");
      //Store selected row
      SelectedRow = $(this);
      if ($(this).find('td').eq(5).text() === "Pending")
      $('#divApplyStatus').show();
      else
      $('#divApplyStatus').hide();
      });
      $("#btnSubmit").click(function (e) {
      $('#Alertpopup').modal('show');
      });
      $("#example tr").hover(function () {
      $(this).css('cursor', 'pointer');
      }, function () {
      $(this).css('cursor', 'auto');
      });
      $("#lnkEdit").click(function (e) {
      window.location.href = "EnquiryDetails.html";
      });
});