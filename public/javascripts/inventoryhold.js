$(document).ready(function () {
            jQuery('#btnLoadSKU').click(function (event) {
            $('#popup').modal('show');
            });
            jQuery('#btnloadbatch').click(function (event) {
            $('#popup').modal('show');
            });
            jQuery('#btnloadQty').click(function (event) {
            $('#popup').modal('show');
            });
            jQuery('#btnloadexpiry').click(function (event) {
            $('#popup').modal('show');
            });
            jQuery('#btnloadmanufacturing').click(function (event) {
            $('#popup').modal('show');
            });
            $("#btnSubmit").click(function (e) {
            $('#Alertpopup').modal('show');
            });
            //checkbox navigaton in table
            $('#chkHeader').change(function () {
            $('#tblOrderDetails tbody tr td input[type="checkbox"]').prop('checked', $(this).prop('checked'));
            });
            $('#tblOrderDetails tbody tr td input:checkbox').change(function () {
            $('#chkHeader').prop('checked', $('td input:checkbox:checked').length == $('td input:checkbox').length);
            });
            //Export table Excel
            jQuery('#btnExportSKUDetails').click(function (event) {
            $('#tblSKUDetails').tableExport({ type: 'excel', escape: 'false' });
            });
            //Export table PDF
            jQuery('#btnExportSKUDetailstoPDF').click(function (event) {
            $('#tblSKUDetails').tableExport({ type: 'pdf', pdfFontSize: '7', escape: 'false' });
            });
            //Export table Excel
            jQuery('#btnExportReceiptDetails').click(function (event) {
            $('#tblOrderDetails').tableExport({ type: 'excel', escape: 'false' });
            });
            //Export table PDF
            jQuery('#btnExportReceiptDetailstoPDF').click(function (event) {
            $('#tblOrderDetails').tableExport({ type: 'pdf', pdfFontSize: '7', escape: 'false' });
            });
            //Take sku details from popup and append to the main table
            $('#btnSubmitpop').click(function () {
            $('#tblSKUDetails').find('tr').not(":first").each(function () {
            if ($(this).find('input[type="checkbox"]').is(':checked')) {
            var sku = $(this).find('td:eq(1)').text();
            var batch = $(this).find('td:eq(3)').text();
            var qty = $(this).find('td:eq(8)').text();
            var manufacturedate = $(this).find('td:eq(6)').text();
            var expirydate = $(this).find('td:eq(7)').text();
            $('#txtFromSKU').val(sku);
            $('#txtFromBatch').val(batch);
            $('#txtFromQty').val(qty);
            $('#txtFromManuDate').val(manufacturedate);
            $('#txtFromExpiryDate').val(expirydate);
            $('#popup').modal('hide');
            }
            });
            });
            var i = 1;
            $('#btnAddDetails').click(function () {
            i++;
            var fromsku = $('#txtFromSKU').val();
            var frombatch = $('#txtFromBatch').val();
            var fromqty = $('#txtFromQty').val();
            var frommanufacturedate = $('#txtFromManuDate').val();
            var fromexpirydate = $('#txtFromExpiryDate').val();
            if (fromsku != '' && frombatch != '' && frommanufacturedate != '' && fromexpirydate != '') {
            var newRow = jQuery('<tr><td><input id="checkbox' + i + '" type="checkbox" /><label for="checkbox' + i + '">Select</label></td><td>' + fromsku + '</td><td>' + frombatch + '</td><td>' + fromexpirydate + '</td><td>' + frommanufacturedate + '</td><td>' + fromqty + '</td></tr>');
            jQuery('#tblOrderDetails').append(newRow);
            $('#txtFromSKU').val('');
            $('#txtFromBatch').val('');
            $('#txtFromQty').val('');
            $('#txtFromManuDate').val('');
            $('#txtFromExpiryDate').val('');
            }
            });
            });